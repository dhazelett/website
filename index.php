<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: http://127.0.0.1');
header('Access-Control-Allow-Method: GET');
header('Access-Control-Allow-Headers: Content-Type');

$clients = [
    [
        'name'     => null,
        'image'    => '',
        'position' => '',
        'length'   => '',
        'markup'   => '',
        'link'     => '',
        'images'   => [],
    ],
    [
        'name'     => 'MotoSport',
        'image'    => '/static/images/clients/motosport/motosport.svg',
        'position' => 'Software Engineer . UI Designer',
        'length'   => '2013 - 2017',
        'markup'   => '',
        'link'     => '/#',
        'images'   => [
            '/static/images/clients/motosport/category-page.png',
            '/static/images/clients/motosport/product-page.png',
            '/static/images/clients/motosport/cart-page.png',
            '/static/images/clients/motosport/checkout-page.png',
        ],
    ],
    [
        'name'     => 'Oregon Blind Cleaners',
        'image'    => '/static/images/clients/obc/logo-black.svg',
        'position' => 'Software Engineer . UI Designer',
        'length'   => '30 Days (2016)',
        'markup'   => '',
        'link'     => '/#',
        'images'   => [
            '/static/images/clients/obc/landing-snapshot.png',
            '/static/images/clients/obc/PCS-booking-capture.png',
        ],
    ],
    [
        'name'     => '32/7',
        'image'    => '/static/images/clients/thirtytwoseven/thirtytwoseven.png',
        'position' => 'Software Engineer . UI Designer',
        'length'   => '2017 - current',
        'markup'   => '',
        'link'     => '/#',
        'images'   => [
            '/static/images/clients/thirtytwoseven/homepage.png',
            '/static/images/clients/thirtytwoseven/about.png',
        ],
    ],
    [
        'name'     => 'KIALOA',
        'image'    => '/static/images/clients/kialoa/kialoa.png',
        'position' => 'Software Engineer . UI Designer',
        'length'   => '2017 - current',
        'markup'   => '',
        'link'     => '/#',
        'images'   => [
            '/static/images/clients/kialoa/ambassadors.png',
            '/static/images/clients/kialoa/about.png',
            '/static/images/clients/kialoa/shopping.png',
            '/static/images/clients/kialoa/stories.png',
        ],
    ],
    [
        'name'     => 'BABYWISE',
        'image'    => '/static/images/clients/babywise/babywise.png',
        'position' => 'Software Engineer . UI Designer',
        'length'   => '2017 - current',
        'markup'   => 'Something somethign somtghin',
        'link'     => '/#',
        'images'   => [],
    ],
];

print json_encode($clients);
