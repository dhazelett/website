import axios from 'axios'

// @todo create url config
const http = axios.create({
  baseURL: 'http://127.0.0.1:8000',
  headers: {
    common: {
      'X-Requested-With': 'XMLHttpRequest'
    }
  }
})

http.interceptors.response.use(
  response => response,
  error => {
    const {response} = error
    return Promise.reject(response)
  }
)

export default function install (Vue) {
  Object.defineProperty(Vue.prototype, '$http', {
    get () {
      return http
    }
  })
}
