import prod from './production'

export const Config = Object.assign(prod, {
  base_uri: 'http://localhost',

  debug: true
});

export default Config;
