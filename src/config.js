let environment = 'production';
if (process.env.NODE_ENV) {
  environment = process.env.NODE_ENV
}

export const Config = require(`./config/${environment}`).default;

export default Config;
