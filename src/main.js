import Vue from 'vue'
import http from 'plugins/http'
import App from '@/App'
import router from '@/router'
import store from 'store'

Vue.use(http)

/* eslint-disable no-new */
new Vue({
  el: '#portfolio',
  router,
  store,
  render: h => h(App)
});
