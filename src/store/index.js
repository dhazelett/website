import Config from '../config'
import Vue from 'vue'
import Vuex from 'vuex'

// actual stores
import client from 'stores/client'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    client
  },
  strict: Config.debug
});
