const RECEIVE_CLIENTS = 'RECEIVE_CLIENTS';

/**
 * Client Store
 */
export default {
  namespaced: true,

  state: {
    all: []
  },

  getters: {
    all (state) {
      return state.all
    }
  },

  actions: {
    all ({ commit }) {
      console.log(this)
      this.$http.get('/')
      // fetch('http://127.0.0.1:8000')
      //   .then(response => response.json())
      //   .then(clients => commit(RECEIVE_CLIENTS, { clients }))
    }
  },

  mutations: {
    [RECEIVE_CLIENTS] (state, { clients }) {
      state.all = clients.filter((client) => client.name !== null)
    }
  }
}
